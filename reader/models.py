from mongoengine import *
from mongoengine.django.auth import User as MongoUser

class Categories(Document):
    title = StringField(required=True)
    user_id = ObjectIdField(required=True)
    date_created = DateTimeField()


class Feeds(Document):
    title = StringField(required=True)
    site_url = URLField()
    link = URLField(required=True)
    last_updated = DateTimeField()
    user_id = ObjectIdField(required=True)
    category = ReferenceField("Categories", dbref=False, reverse_delete_rule=CASCADE)


class Posts(Document):
    title = StringField(required=True)
    is_important = IntField()
    is_viewed = IntField()
    read_later = IntField()
    image = StringField()
    summary = StringField(required=True)
    feed = ReferenceField("Feeds", dbref=False, reverse_delete_rule=CASCADE)
    user_id = ObjectIdField(required=True)
    link = StringField(required=True)
    tags = ListField()
    date_created = DateTimeField()
    date_added = DateTimeField()

class User(MongoUser):
    favorite_tags = ListField()

class FeedsStatistic(Document):
    feed = ReferenceField("Feeds", dbref=False, reverse_delete_rule=CASCADE)
    date = StringField()
    count_posts = IntField()
    hours_details = DictField()
    date_last_update = DateTimeField()