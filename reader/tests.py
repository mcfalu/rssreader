import json

from django.test.client import Client
from django.utils.http import urlquote
from mongoengine.django.auth import User

from reader.utils.test import CustomTestCase
from reader import models


class UrlTest(CustomTestCase):

    def setUp(self):
        #get info about test user
        user = User.objects.filter(username="test-user").limit(1).first()
        self._user_id = user['id'].__str__()
        self._username = 'test-user'
        self._password = 'test-user'

        self._url = [
            '/',
            '/profile/',
        ]

    def _get_logged_in_client(self):
        client = Client()

        #login to the system
        client.login(username=self._username, password=self._password)

        return client

    def _test_not_logged_in_url(self, url):

        # check status of response code for user that isn't logged in
        self.assertRedirects(Client().get(url), '/login/?next=' + urlquote(url))

    def _test_logged_in_url(self, url):

        c = self._get_logged_in_client()

        # check status code of response for user that is logged in
        self.assertEqual(c.get(url).status_code, 200)

    # Test access to URLS for not logged in and logged in user
    def test_urls(self):

        for url in self._url:
            self._test_not_logged_in_url(url)
            self._test_not_logged_in_url(url)

    # Test action of successfully adding category
    def test_category_add_success(self):

        c = self._get_logged_in_client()

        title = 'Category title'
        response = c.post('/categories/add/', {'title': title})

        self.assertEqual(response.status_code, 302)

    # Test action of failed adding category
    def test_category_add_failed(self):

        c = self._get_logged_in_client()

        title = ''
        response = c.post('/categories/add/', {'title': title})

        #status code of resonse should be 200
        self.assertEqual(response.status_code, 200)

        #form should containt some alert with error message
        self.assertContains(response, 'class="errorlist"')

    # Test action of successfully editing category
    def test_category_edit_success(self):

        c = self._get_logged_in_client()

        category_instance = models.Categories.objects.first()

        new_title = 'Edited category'
        url = '/categories/edit/{0}/'.format(category_instance.id)

        response = c.post(url, {'title': new_title})

        self.assertEqual(response.status_code, 302)

    # Test action of failed editing category
    def test_category_edit_failed(self):

        c = self._get_logged_in_client()

        category_instance = models.Categories.objects.first()

        new_title = ''
        url = '/categories/edit/{0}/'.format(category_instance.id)

        response = c.post(url, {'title': new_title})

        self.assertEqual(response.status_code, 200)

    # Test action of successfully deleting category
    def test_category_remove_success(self):

        c = self._get_logged_in_client()

        category_instance = models.Categories.objects.first()

        url = '/categories/delete/{0}/'.format(category_instance.id)

        response = c.post(url)

        self.assertJSONContain(response.content, {"success": True})


    # Test action of successfully adding feed
    def test_feed_add_success(self):

        c = self._get_logged_in_client()

        category_instance = models.Categories(title='Test category',
            user_id=self._user_id)

        category_instance.save()

        response = c.post('/feeds/add/', {
            'title': 'First feed',
            'category' : category_instance.id,
            'site_url' : 'http://habrahabr.ru/',
            'link' : 'http://habrahabr.ru/rss/hubs/'
            })

        self.assertEqual(response.status_code, 302)


    # Test action of failed adding feed
    def test_feed_add_failed(self):
        c = self._get_logged_in_client()

        # try to add feed without category
        response = c.post('/feeds/add/', {
            'title': 'First feed',
            'site_url' : 'http://habrahabr.ru/',
            'link' : 'http://habrahabr.ru/rss/hubs/'
            })

        self.assertEqual(response.status_code, 200)


    # Test action of success editing feed
    def test_feed_edit_success(self):

        c = self._get_logged_in_client()
        feed_instance = models.Feeds.objects.first()

        new_title = 'Edited feed title'
        url = '/feeds/edit/{0}/'.format(feed_instance.id)

        response = c.post(url, {
            'title': new_title,
            'category' : feed_instance.category.id,
            'site_url' : feed_instance.site_url,
            'link' : feed_instance.link
            })

        self.assertEqual(response.status_code, 302)


    # Test action of failed editing feed
    def test_feed_edit_failed(self):

        c = self._get_logged_in_client()
        feed_instance = models.Feeds.objects.first()

        new_title = 'Edited feed title'
        url = '/feeds/edit/{0}/'.format(feed_instance.id)

        #try to submit olny title value
        response = c.post(url, {
            'title': new_title,
            })

        self.assertEqual(response.status_code, 200)


    # Test success removing feed
    def test_feed_remove_success(self):

        c = self._get_logged_in_client()

        feed_instance = models.Feeds.objects.first()

        url = '/feeds/delete/{0}/'.format(feed_instance.id)

        response = c.post(url)

        self.assertJSONContain(response.content, {"success": True})


    # Test success changing user's password
    def test_change_user_password_success(self):

        username = 'test-user1'
        password = 'test-password'

        u = User()
        u.username = username
        u.set_password(password)
        u.save()

        c = Client()
        c.login(username=username, password=password)

        response = c.post('/change-password/', {
            'old_password' : password,
            'new_password' : 'test-user-new-password',
            'new_password_confirmation' : 'test-user-new-password'
            })

        self.assertContains(response, "Password has been successfully updated")


    # Test success changing username
    def test_change_user_username_success(self):

        username = 'test-user2'
        password = 'test-password'

        u = User()
        u.username = username
        u.set_password(password)
        u.save()

        c = Client()
        c.login(username=username, password=password)

        response = c.post('/change-username/', {
            'username' : 'new-username',
            })

        self.assertContains(response, "Username has been successfully updated")


    # Test changing username that is already used by other user
    def test_change_user_username_failed(self):

        username = 'test-user3'
        password = 'test-password'

        u = User()
        u.username = username
        u.set_password(password)
        u.save()

        u = User()
        u.username = 'test-user4'
        u.set_password('test-password')
        u.save()

        c = Client()
        c.login(username=username, password=password)

        response = c.post('/change-username/', {
            'username' : 'test-user4',
            })

        self.assertContains(response, "This username is already used")