from reader import models
from reader.core.feeds import get_feeds


# Get list of categories
def get_categories(user_id, is_get_feeds=True):

    result = {}

    if None is user_id:
        return result

    categories_list = models.Categories.objects.filter(user_id=user_id)
    categories_list = categories_list.order_by("id")

    if None is not categories_list:

        for category_item in categories_list:
            category_id = category_item['id'].__str__()

            result[category_id] = {
                "id": category_id,
                "title": category_item["title"],
                "feeds": {},
                "count_unviewed": 0
            }

    if False is is_get_feeds:
        return result

    #get list of feeds
    feeds_list = get_feeds(user_id, get_count_unviewed=True)

    for feed_id in feeds_list:

        feed_item = feeds_list[feed_id]
        category_id = feed_item["category"]["id"].__str__()

        if category_id in result:
            result[category_id]["feeds"][feed_id] = feed_item
            result[category_id]["count_unviewed"] += feed_item.count_unviewed

    return result