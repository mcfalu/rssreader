from datetime import datetime, timedelta
import calendar

from reader import models

def get_feeds_info_by_date(date):

    start = datetime.strptime(date + " 00:00:00", "%Y-%m-%d %H:%M:%S")
    end = datetime.strptime(date + " 23:59:59", "%Y-%m-%d %H:%M:%S")

    posts = models.Posts.objects.only("feed", "date_created")
    posts = posts.filter(date_created__gte=start, date_created__lte=end)

    result = {}
    for post in posts:

        feed_id = post['feed']['id'].__str__()

        if feed_id not in result:

            result[feed_id] = dict(count_posts=0, hours_details={}, feed=post['feed'])

        result[feed_id]['count_posts'] = result[feed_id]['count_posts'] + 1

        hour = post.date_created.hour
        hour = str(hour)
        if hour not in result[feed_id]['hours_details']:
            result[feed_id]['hours_details'][hour] = 0

        result[feed_id]['hours_details'][hour] = result[feed_id]['hours_details'][hour] + 1

    return result

def get_feeds_period_info_total(user_id, date_start, date_end):

    posts_statistic = models.FeedsStatistic._get_collection().aggregate([
        {"$match": {"date": {"$gte" : date_start, "$lte" : date_end}}},
        {"$group": {"_id": "$date", "count": {"$sum": "$count_posts"}}},
    ])

    date_start = datetime.strptime(date_start + " 00:00:00", "%Y-%m-%d %H:%M:%S")
    date_end = datetime.strptime(date_end + " 23:59:59", "%Y-%m-%d %H:%M:%S")

    posts_by_dates = {}

    if len(posts_statistic['result'])>0:

        for post in posts_statistic['result']:

            date = post['_id']

            if date not in posts_by_dates:
                posts_by_dates[date] = 0

            posts_by_dates[date] = posts_by_dates[date] + post['count']

    result = []

    while date_start.date() <= date_end.date():

        day = date_start.strftime("%d")
        date = date_start.strftime("%Y-%m-%d")

        day_info = {
            "title" : day,
            "value" : 0
        }

        if date in posts_by_dates:
            day_info["value"] = posts_by_dates[date]

        result.append(day_info)

        date_start = date_start + timedelta(days=1)

    return result


def get_feeds_period_info(user_id, date_start, date_end):

    posts_statistic = models.FeedsStatistic._get_collection().aggregate([
        {"$match": {"date": {"$gte" : date_start, "$lte" : date_end}}},
        {"$group": {"_id": "$feed", "count": {"$sum": "$count_posts"}}},
        {"$sort" : {"count" : -1}}
    ])

    result = []
    if len(posts_statistic['result'])>0:

        from reader.core.feeds import get_feeds
        feeds = get_feeds(user_id=user_id)

        for item in posts_statistic['result']:

            feed_id = item["_id"].__str__()

            feed_info = {
                "title" : "",
                "site_url" : "",
                "count" : item["count"]
            }

            if feed_id in feeds:

                feed_info["title"] = feeds[feed_id]["title"]
                feed_info["site_url"] = feeds[feed_id]["site_url"]

            result.append(feed_info)

    return result


def get_feeds_year_info_total(user_id, year):

    date_start = year + '-01-01'
    date_end = year + '-12-31'

    posts_statistic = models.FeedsStatistic._get_collection().aggregate([
        {"$match": {"date": {"$gte" : date_start, "$lte" : date_end}}},
        {"$group": {"_id": "$date", "count": {"$sum": "$count_posts"}}},
    ])

    date_start = datetime.strptime(date_start + " 00:00:00", "%Y-%m-%d %H:%M:%S")
    date_end = datetime.strptime(date_end + " 23:59:59", "%Y-%m-%d %H:%M:%S")

    posts_by_month = {}

    if len(posts_statistic['result'])>0:

        for post in posts_statistic['result']:

            date = post['_id']
            date = datetime.strptime(date, "%Y-%m-%d")
            date_month = int(date.strftime("%m").lstrip('0'))

            if date_month not in posts_by_month:
                posts_by_month[date_month] = 0

            posts_by_month[date_month] = posts_by_month[date_month] + post['count']

    result = []

    for month in xrange(1,13):

        month_info = {
            "title" : calendar.month_abbr[month],
            "value" : 0
        }

        if month in posts_by_month:
            month_info["value"] = month_info["value"] + posts_by_month[month]

        result.append(month_info)

    return result


def get_feeds_year_info(user_id, year):

    date_start = year + '-01-01'
    date_end = year + '-12-31'

    posts_statistic = models.FeedsStatistic._get_collection().aggregate([
        {"$match": {"date": {"$gte" : date_start, "$lte" : date_end}}},
        {"$group": {"_id": "$feed", "count": {"$sum": "$count_posts"}}},
        {"$sort" : {"count" : -1}}
    ])

    result = []
    if len(posts_statistic['result'])>0:

        from reader.core.feeds import get_feeds
        feeds = get_feeds(user_id=user_id)

        for item in posts_statistic['result']:

            feed_id = item["_id"].__str__()

            feed_info = {
                "title" : "",
                "site_url" : "",
                "count" : item["count"]
            }

            if feed_id in feeds:

                feed_info["title"] = feeds[feed_id]["title"]
                feed_info["site_url"] = feeds[feed_id]["site_url"]

            result.append(feed_info)

    return result


def update_feed_date_statistic(feed_info, date):

    feed_id = feed_info['feed']['id'].__str__()
    statistic_item = models.FeedsStatistic.objects(feed=feed_id, date=date).first()

    try:

        if None == statistic_item:

            statistic_item = models.FeedsStatistic(feed=feed_info['feed'],
                date=date,
                count_posts=feed_info['count_posts'],
                hours_details=feed_info['hours_details'])

            statistic_item.save()

        else:

            statistic_item.count_posts = feed_info['count_posts']
            statistic_item.hours_details = feed_info['hours_details']
            statistic_item.save()

    except Exception, e:

        print "Error when trying to update date statistic for feed {0}".format(feed_id)