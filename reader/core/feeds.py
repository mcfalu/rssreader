import datetime
from urllib2 import *
import logging
import HTMLParser
from BeautifulSoup import BeautifulSoup

import feedparser
import settings

from reader import models
from reader.core.statistic import get_feeds_info_by_date, update_feed_date_statistic

# Get list of feeds
def get_feeds(user_id, category_id=None, get_count_unviewed=False):

    result = {}

    if None is user_id:
        return result

    feeds_list = models.Feeds.objects.filter(user_id=user_id)

    if None != category_id and ''!= category_id:
        category = models.Categories.objects(id=category_id).first()
        feeds_list = feeds_list.filter(category=category)

    feeds_list = feeds_list.order_by("-id")

    if None is not feeds_list:

        for feed_item in feeds_list:
            feed_item.count_unviewed = 0
            feed_id = feed_item['id'].__str__()

            result[feed_id] = feed_item

    #get info about count unviewed items
    if get_count_unviewed:

        unviewed_posts_info = get_count_unviewed_posts()

        for item in unviewed_posts_info:

            feed_id = item['feed_id']
            if feed_id in result:
                result[feed_id].count_unviewed = item['count']

    return result


# Get count of unviewed post by feeds
def get_count_unviewed_posts():

    unviewed_posts_info = models.Posts._get_collection().aggregate([
        {"$match": {"is_viewed": 0}},
        {"$group": {"_id": "$feed", "count": {"$sum": 1}}},
    ])

    if 'result' in unviewed_posts_info:

        result = []

        for item in unviewed_posts_info['result']:
            result.append({
                'feed_id': item['_id'].__str__(),
                'count': item['count']
            })

        return result


# Check if exist feed
def is_exist_feed(feed_link, feed_id, user_id):

    feed_info = models.Feeds.objects(link=feed_link, user_id=user_id).first()
    if None is feed_info:
        return False

    #if isn't specified feed id
    if None is feed_id:
        return True
    #if we find feed with same feed_id
    elif feed_info.id.__str__() == feed_id:
        return False
    else:
        return True


# Get info about feed
def get_feed(feed_id, user_id=None):

    try:
        feed_info = models.Feeds.objects(id=feed_id)

        if None != user_id:
            feed_info = feed_info.filter(user_id=user_id)

        feed_info = feed_info.first()
        return feed_info

    except Exception, e:
        return None

def update_feeds(log=False, category_id=None, feed_id=None, user_id=None):

    logger = logging.getLogger(__name__)

    feeds_items = models.Feeds.objects()

    if None is not feed_id:
        feeds_items = feeds_items.filter(id=feed_id)
    elif None is not category_id:
        feeds_items = feeds_items.filter(category=category_id)

    if None is not user_id:
        feeds_items = feeds_items.filter(user_id=user_id)

    for feed in feeds_items:

        if log:
            logger.info("Starting to update feed: " + feed['title'])

        user = models.User.objects(id=feed['user_id']).first()
        user_fav_tags = []
        if 'favorite_tags' in user:
            user_fav_tags = user.favorite_tags

        if ping_url(feed['link']):

            d = feedparser.parse(feed['link'])

            count_new_posts = 0

            for entry in d['entries']:

                #check if there is already exist post with such link
                if None is models.Posts.objects(link=entry['link'], user_id=feed['user_id']).first():

                    is_important = 0
                    tags = []
                    if 'tags' in entry:

                        for tag in entry['tags']:

                            tags.append(tag['term'])

                            if tag['term'] in user_fav_tags:
                                is_important = 1

                    #save post to database
                    if 'created_parsed' in entry:
                        date_created = entry['created_parsed']
                    else:
                        date_created = entry['published_parsed']

                    image = ''
                    if 'enclosures' in entry:
                        image = get_image(entry['enclosures'])

                    post = models.Posts(title=HTMLParser.HTMLParser().unescape(entry['title']),
                                        user_id=feed['user_id'],
                                        link=entry['link'],
                                        feed=feed,
                                        is_viewed=0,
                                        is_important=is_important,
                                        tags=tags,
                                        image=image,
                                        date_created=datetime.datetime(*date_created[0:6]),
                                        date_added=datetime.datetime.utcnow())

                    post.summary = ''
                    if 'summary' in entry:
                        post.summary = strip_tags(entry['summary'], settings.ALLOWED_HTML_TAGS_IN_SUMMARY)

                    if post.save(write_concern={'fsync': True}):

                        count_new_posts += 1

            #update info about count of unviewed posts
            if not 'count_unviewed' in feed:
                feed.count_unviewed = 0

            feed.count_unviewed += count_new_posts
            feed.last_updated = datetime.datetime.utcnow()

            feed.save()

            if log:
                logger.info("Count of new posts: {0}".format(count_new_posts))

    date = datetime.datetime.now().strftime("%Y-%m-%d")

    date_statistic_info = get_feeds_info_by_date(date)
    for feed_id in date_statistic_info:

        feed_info = date_statistic_info[feed_id]
        update_feed_date_statistic(feed_info, date)

def get_image(enclosures):

    image_types = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif']

    for item in enclosures:

        if 'type' in item and item.type in image_types:
            return item.href

    return ''

def ping_url(url, timeout=5):

    req = Request(url, headers={ 'User-Agent' : 'Mozilla/5.0' })

    # Try to open the url
    try:
        reponse = urlopen(req, timeout = timeout)

        return True
    except Exception, e:

        logger = logging.getLogger(__name__)
        logger.info("Couldn't connect to {0}".format(url))

        print e

        return False

def strip_tags(html, whitelist=[]):
    """
    Strip all HTML tags except for a list of whitelisted tags.
    """
    soup = BeautifulSoup(html)

    for tag in soup.findAll(True):
        if tag.name not in whitelist:
            tag.append(' ')
            tag.replaceWithChildren()

    result = unicode(soup)

    # Clean up any repeated spaces and spaces like this: '<a>test </a> '
    result = re.sub(' +', ' ', result)
    result = re.sub(r' (<[^>]*> )', r'\1', result)
    return result.strip()