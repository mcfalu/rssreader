from reader.utils.local import localtime
from reader import models
from reader.core import feeds

import settings


# Get list of posts
def get_posts(limit=None,
              offset=0,
              feed_id=None,
              category_id=None,
              is_read_later=None,
              last_post_id=None,
              is_viewed=None,
              search_string='',
              is_important=None,
              user_id=None):

    result = []

    posts = models.Posts.objects(user_id=user_id)

    if None is not feed_id:
        posts = posts.filter(feed=feed_id)

    elif None is not category_id:

        feeds_id = []
        category_feeds = models.Feeds.objects(category=category_id)

        for feed_info in category_feeds:
            feeds_id.append(feed_info['id'])

        posts = posts.filter(feed__in=feeds_id)

    if None is not last_post_id:
        posts = posts.filter(id__gt=last_post_id)

    if None is not is_viewed:
        posts = posts.filter(is_viewed=is_viewed)

    if None is not is_read_later:
        posts = posts.filter(read_later=1)

    if None is not is_important:
        posts = posts.filter(is_important=is_important)

    if '' != search_string:

        tag_pos = search_string.find("tag:")

        if tag_pos == -1:
            posts = posts.filter(title__icontains=search_string)
        else:

            tags = [search_string[(tag_pos + 4):]]

            posts = posts.filter(tags__in=tags)

    posts = posts.order_by("-date_created")

    if None is not limit:
        posts = posts.limit(limit)

    if 0 != offset:
        posts = posts.skip(offset)

    if None != posts:

        #get info about feeds
        feeds_list = feeds.get_feeds(user_id=user_id)

        for post in posts:

            post_id = post['id'].__str__()
            post_feed_id = post['feed']['id'].__str__()
            post_category_id = post['feed']['category']['id'].__str__()

            post_info = {
                'id': post_id,
                'category_id': post_category_id,
                'title': post['title'],
                'link': post['link'],
                'tags': post['tags'],
                'image' : post['image'],
                'read_later': 0,
                'is_viewed': post['is_viewed'],
                'date_created': post['date_created'],
                'date_added': post['date_added'].__str__(),
                'feed_id': post_feed_id,
                'feed_title': ''
            }

            if 'read_later' in post:
                post_info['read_later'] = post['read_later']

            if post_feed_id in feeds_list:

                feed_info = feeds_list[post_feed_id]

                post_info['feed_title'] = feed_info['title']
                post_info['feed_site_url'] = feed_info['site_url']

                result.append(post_info)

    return result


# Get max id of post
def get_last_id():

    try:
        post = models.Posts.objects.order_by("-id").limit(1).first()
        return post['id'].__str__()
    except Exception, e:
        return ''


# Format posts content for output
def format_posts(posts_items):

    if 0 != len(posts_items):

        for i in range(0, len(posts_items)):

            date_created = posts_items[i]['date_created']
            date_created = localtime(date_created)
            date_created = date_created.strftime(settings.DATETIME_FORMAT)
            posts_items[i]['date_created'] = date_created

    return posts_items


# Mark as read
def mark_as_read(post_id=None, feed_id=None, category_id=None, is_important=None, user_id=None):

    try:

        posts = models.Posts.objects(user_id=user_id)

        if None != feed_id and ''!=feed_id:

            posts = posts.filter(feed=feed_id)

        elif None is not category_id:

            feeds_id = []
            category_feeds = models.Feeds.objects(category=category_id)

            for feed_info in category_feeds:
                feeds_id.append(feed_info['id'])

            posts = posts.filter(feed__in=feeds_id)

        if None != post_id and '' != post_id:
            posts = posts.filter(id=post_id)

        if None != is_important and '' != is_important:
            posts = posts.filter(is_important=is_important)

        posts.all().update(set__is_viewed=1)

        return True

    except Exception, e:
        return False


def read_later(post_id=None, user_id=None, is_read_later=1):

    try:

        posts = models.Posts.objects(user_id=user_id, id=post_id)
        posts.update(set__read_later=is_read_later)

        return True

    except Exception, e:
        return False


def get_post(post_id, user_id):

    try:
        post = models.Posts.objects(user_id=user_id, id=post_id).limit(1).first()
        return post

    except Exception, e:
        return None

# Get count of unviewed post by feeds
def get_count_important_posts(is_viewed=None):

    data = models.Posts.objects(is_important=1)
    if None != is_viewed:
        data = data.filter(is_viewed=is_viewed)

    return data.count()