from django.conf.urls import patterns, url

from reader.views import home, posts, feeds, account, category, tags, statistic

urlpatterns = patterns('',

    url(r'^$', home.index, name='index'),
    url(r'^posts/$', posts.items),
    url(r'^posts/(.*)$', posts.item),
    url(r'^posts-mark-as-read/$', posts.mark_as_read),
    url(r'^login/$', account.login),
    url(r'^logout/$', account.logout),
    url(r'^profile/$', account.profile),
    url(r'^change-password/$', account.change_password),
    url(r'^change-username/$', account.change_username),
    url(r'^statistic/$', statistic.index),
    url(r'^statistic/tags/$', statistic.tags),
    url(r'^statistic/data/(.*)/$', statistic.data),
    # Categories
    url(r'^categories/$', category.categories),
    url(r'^categories/add/$', category.add),
    url(r'^categories/edit/(.*)/$', category.edit),
    url(r'^categories/delete/(.*)/$', category.delete),
    # Feeds
    url(r'^feeds/$', feeds.feeds),
    url(r'^feeds/add/$', feeds.add),
    url(r'^feeds/edit/(.*)/$', feeds.edit),
    url(r'^feeds/delete/(.*)/$', feeds.delete),
    # Favorite tags
    url(r'^favorite-tags/$', tags.favorite),
    url(r'^profile/tags/$', tags.items),
    url(r'^profile/tags/(.*)$', tags.item),
)