"""Support for testing."""

from django.test.simple import DjangoTestSuiteRunner
from mongoengine.django.auth import User

from reader import models


class CustomTestRunner(DjangoTestSuiteRunner):

    def setup_databases(self):

        print "Init database data"

        username = 'test-user'
        password = 'test-user'

        u = User()
        u.username = username
        u.set_password(password)
        u.save()

        print " - created user username: '{0}' and password: '{1}'".format(username, password)

    def teardown_databases(self, *args):

        #Remove all items from collection User
        User.objects().delete()

        print " -removed all documents from collection 'user'"

        models.Categories.objects().delete()

        print " -removed all documents from collection 'categories'"

        #Remove all items from collection Feeds
        models.Feeds.objects().delete()

        print " -removed all documents from collection 'feeds'"

        #Remove all items from collection Posts
        models.Posts.objects().delete()

        print " -removed all documents from collection 'posts'"