import simplejson

from django.http import QueryDict

# Get request param value
def get_param(request, name, default=None):

    params = dict(simplejson.loads(request.body))
    if name in params:
        return params[name]

    return default
