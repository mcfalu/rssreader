import settings
import pytz

def localtime(datetime):

    local_tz = pytz.timezone(settings.TIME_ZONE)
    return datetime.replace(tzinfo=pytz.utc).astimezone(local_tz).replace(tzinfo=None)