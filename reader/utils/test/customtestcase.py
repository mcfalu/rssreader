from django.test import TestCase
import json

class CustomTestCase(TestCase):

    def assertJSONContain(self, raw, expected_data):
        try:
            data = json.loads(raw)
        except ValueError:
            self.fail("First argument is not valid JSON: %r" % raw)

        for key in expected_data:

            if key not in data:
                self.fail("Value with key \"{0}\" isn't in responce. Content: {1}".format(key, raw))
            elif data[key] != expected_data[key]:
                self.fail("Values with key \"{0}\" aren't equal ({1} != {2}). Content: {3}".format(key, data[key], expected_data[key], raw))