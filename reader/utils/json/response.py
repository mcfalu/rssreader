from django.http import HttpResponse
from django.utils import simplejson

def json_response(something):
	
    return HttpResponse(
        simplejson.dumps(something),
        content_type = 'application/json; charset=utf8'
    )