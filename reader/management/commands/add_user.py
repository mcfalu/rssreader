from optparse import make_option

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from reader.models import User

class Command(BaseCommand):

    # db.user.update({}, { "$set":{_cls:"User.User"}}, {multi: true})

    help = "Adding new user"

    option_list = BaseCommand.option_list + (
        make_option(
            "-l",
            "--login",
            dest = "login",
            help = "User's login",
        ),
    )

    option_list = option_list + (
        make_option(
            "-p",
            "--password",
            dest = "password",
            help = "User's password"
        ),
    )

    option_list = option_list + (
        make_option(
            "-e",
            "--email",
            dest = "email",
            help = "User's email"
        ),
    )

    def handle(self, *args, **options):

        if options['login'] == None :
            raise CommandError("Please input login of user")

        username = options['login']

        #check if is already exist user with this login
        user = User.objects(username=username).first()
        if None is not user:
            raise CommandError("User with this login is already exist in system")

        if options['password'] == None :
            raise CommandError("Please input password of user")

        password = options['password']

        if options['email'] == None :
            raise CommandError("Please input valid email")

        email = options['email']

        try:
            validate_email(email)
        except ValidationError as e:

            raise CommandError("Please input valid email")

        #check if is already exist user with this email
        user = User.objects(email=email).first()
        if None is not user:

            raise CommandError("User with this email is already exist in system")

        try:

            User.create_user(username, password, email)

            print "User with username: {0} has been successfully added".format(username)

        except Exception, e:

            raise CommandError("Error when trying to add user to the system")