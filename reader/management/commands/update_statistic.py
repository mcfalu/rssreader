from datetime import datetime, timedelta

from django.core.management.base import NoArgsCommand, CommandError

from reader.core.statistic import get_feeds_info_by_date, update_feed_date_statistic
from reader import models

class Command(NoArgsCommand):

    help = "Updating date statistic on period"

    def handle_noargs(self, **options):

        date_start = datetime.strptime("2013-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")
        date_end = datetime.strptime("2014-07-26 23:59:59", "%Y-%m-%d %H:%M:%S")

        while date_start.date() <= date_end.date():

            date = date_start.strftime("%Y-%m-%d")

            print "Start update statistic for date {0}".format(date)

            date_statistic_info = get_feeds_info_by_date(date)
            for feed_id in date_statistic_info:

                feed_info = date_statistic_info[feed_id]
                update_feed_date_statistic(feed_info, date)

            date_start = date_start + timedelta(days=1)