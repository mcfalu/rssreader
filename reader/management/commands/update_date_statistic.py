from optparse import make_option
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from reader.core.statistic import get_feeds_info_by_date, update_feed_date_statistic
from reader import models

class Command(BaseCommand):

    help = "Updating date statistic"

    option_list = BaseCommand.option_list + (
        make_option(
            "-d",
            "--date",
            dest = "date",
            help = "Date",
        ),
    )

    def handle(self, *args, **options):

        date = datetime.today().strftime("%Y-%m-%d")
        if options['date'] != None :

            try:
                datetime.strptime(options['date'], '%Y-%m-%d')
                date = options['date']

            except ValueError:
                raise ValueError("Incorrect date format, should be YYYY-MM-DD")

        print "Start update statistic for date {0}".format(date)

        date_statistic_info = get_feeds_info_by_date(date)
        for feed_id in date_statistic_info:

            feed_info = date_statistic_info[feed_id]
            update_feed_date_statistic(feed_info, date)