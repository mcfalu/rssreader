from django.core.management.base import NoArgsCommand

from reader.core.feeds import update_feeds

class Command(NoArgsCommand):
    help = "Updating rss feeds"

    def handle_noargs(self, **options):
        update_feeds(log=True)