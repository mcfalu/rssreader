import datetime
import json

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.html import strip_tags

from reader.utils.json import json_response
from reader import models
from reader.views.feeds import get_menu_items
from reader.core.categories import get_categories
from reader.forms.category import CategoryForm

#Page with lists of feeds
@login_required
def categories(request):

    categories = get_categories(user_id=request.user.id)

    context = {
        'categories': categories,
        'menu_items' : get_menu_items()
    }
    return render(request, 'templates/categories/list.html', context)

@login_required
def add(request):

    if request.method=='POST':

        form = CategoryForm(request.POST)

        if form.is_valid():

            title = form.cleaned_data['title'].strip()
            title = strip_tags(title)

            color = form.cleaned_data['color'].strip()

            try:

                category = models.Categories(title=title,
                                             user_id=request.user.id,
                                             color=color)
                category.save()

                return HttpResponseRedirect('/categories/')

            except Exception, e:
                pass
    else:
        form = CategoryForm()

    context = {
        'menu_items' : get_menu_items(),
        'form' : form
    }

    return render(request, 'templates/categories/add.html', context)

#Editing existing category
@login_required
def edit(request, category_id):

    category_instance = models.Categories.objects(id=category_id,
                                                  user_id=request.user.id).first()

    if None == category_instance:
        raise Http404

    if request.method=='POST':

        form = CategoryForm(request.POST)

        if form.is_valid():

            title = form.cleaned_data['title'].strip()
            title = strip_tags(title)

            color = form.cleaned_data['color'].strip()

            try:

                category_instance.title = title
                category_instance.color = color

                category_instance.save()

                return HttpResponseRedirect('/categories/')

            except Exception, e:
                pass
    else:

        form = CategoryForm(initial=dict(title=category_instance.title))

    context = {
        'menu_items' : get_menu_items(),
        'form' : form
    }

    return render(request, 'templates/categories/edit.html', context)

#Delete existing feed
@login_required
def delete(request, category_id):

    category_instance = models.Categories.objects(id=category_id,
                                                  user_id=request.user.id).first()

    if None == category_instance:
        raise Http404

    try:

        category_instance.delete()

        return json_response({
            "success": True,
        })

    except Exception, e:

        return json_response({
            "success": False,
        })