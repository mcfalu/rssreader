from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotFound, HttpResponse
from django.conf import settings

from reader.core.feeds import update_feeds, get_count_unviewed_posts
from reader.utils.json import json_response
from reader.core import posts
from reader.utils.request import get_param

# Get posts
@login_required
def items(request):

    user_id = request.user.id
    is_viewed = request.GET.get('is_viewed', None)
    feed_id = request.GET.get('feed_id', None)
    category_id = request.GET.get('category_id', None)
    limit = int(request.GET.get('limit', settings.LIMIT_POSTS))
    offset = int(request.GET.get('offset', 0))
    search_string = request.GET.get('search', '')
    read_later = request.GET.get('read_later', None)
    last_post_id = request.GET.get('last_post_id', None)
    is_important = request.GET.get('is_important', None)

    if '' == last_post_id:
        last_post_id = None

    if '1'==read_later:
        is_viewed = None

    if '1' == request.GET.get('force_update', None):
        # Update feeds
        update_feeds(category_id=category_id, feed_id=feed_id, user_id=user_id)

    posts_items = posts.get_posts(is_viewed=is_viewed,
                                  feed_id=feed_id,
                                  category_id=category_id,
                                  is_read_later=read_later,
                                  limit=limit,
                                  offset=offset,
                                  search_string=search_string,
                                  last_post_id=last_post_id,
                                  is_important=is_important,
                                  user_id=user_id)

    result = {
        "posts": posts.format_posts(posts_items),
        "last_post_id": posts.get_last_id()
    }

    if '1' == request.GET.get('force_update', None):
        result["count_unviewed_posts"] = get_count_unviewed_posts()
    else:
        result["next_offset"] = offset + limit

    return json_response(result)

#Get post
@login_required
def item(request, post_id):

    post_info = posts.get_post(post_id=post_id, user_id=request.user.id)

    if None != post_info:

        # partitial item update
        if request.method == 'PATCH':

            is_viewed = get_param(request, 'is_viewed', None)
            if None != is_viewed:
                post_info.is_viewed = is_viewed

            read_later = get_param(request, 'read_later', None)
            if None != read_later:
                post_info.read_later = read_later

            success = False

            try:
                post_info.save()
                success = True

            except Exception, e:
                pass

            return json_response({'success' : success})

        # get info abou item
        elif request.method == 'GET':

            post_info = {
                'id': post_id,
                'category_id': post_info['feed']['category']['id'].__str__(),
                'title': post_info['title'],
                'link': post_info['link'],
                'tags': post_info['tags'],
                'image' : post_info['image'],
                'read_later': post_info['read_later'],
                'summary' : post_info['summary'],
                'is_viewed': post_info['is_viewed'],
                'is_important' : post_info['is_important'],
                'date_created': post_info['date_created'].__str__(),
                'date_added': post_info['date_added'].__str__(),
                'feed_id': post_info['feed']['id'].__str__(),
            }

            return json_response(post_info)
        else:

            return HttpResponse(content='Not allowed request method', status=405)


    return HttpResponseNotFound('Not found post with id {0}'.format(post_id))

# Mark as read
@login_required
def mark_as_read(request):

    category_id = request.POST.get('category_id', None)
    feed_id = request.POST.get('feed_id', None)
    is_important = request.POST.get('is_important', None)
    user_id = request.user.id

    result = {
        "success": posts.mark_as_read(category_id=category_id,
                                      feed_id=feed_id,
                                      is_important=is_important,
                                      user_id=user_id)
    }

    if True == result['success']:
        result["count_unviewed_posts"] = get_count_unviewed_posts()
        result["total_important"] = posts.get_count_important_posts(is_viewed=0),

    return json_response(result)