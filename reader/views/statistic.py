from datetime import datetime
from datetime import date
import calendar

from django.shortcuts import render
from django.contrib import auth
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from reader.forms.statistic import FilterForm

from reader.utils.json import json_response
from reader.core import statistic

#List of menu items in sidebar
def get_menu_items():

    result = [{
        'url' : '/statistic/',
        'current' :  '^/statistic/$',
        'title' : 'Feeds'
    }]

    return result


@login_required
def index(request):

    filter_form = FilterForm(initial={
        "month" : datetime.now().month,
        "year": datetime.now().year
        })

    context = {
        "menu_items" : get_menu_items(),
        "filter_form" : filter_form
    }


    return render(request, 'templates/statistic/index.html', context)

@login_required
def tags(request):

    context = {
        "menu_items" : get_menu_items()
    }

    return render(request, 'templates/statistic/tags.html', context)

@login_required
def data(request, mode):

    data = {}

    if 'month' == mode:

        month = datetime.now().month
        if None != request.POST.get("month", None):
            month = request.POST.get("month")

        year = datetime.now().year
        if None != request.POST.get("year", None):
            year = request.POST.get("year")

        d = date(int(year), int(month), 1)
        days_in_month = calendar.monthrange(int(year), int(month))[1]

        date_start = d.strftime("%Y-%m-%d")
        date_end = d.strftime("%Y-%m-")+str(days_in_month)

        data_summary = statistic.get_feeds_period_info_total(user_id=request.user.id, date_start=date_start, date_end=date_end)
        data_details = {
            "feeds" : statistic.get_feeds_period_info(user_id=request.user.id, date_start=date_start, date_end=date_end)
        }

    elif 'year' == mode:

        year = datetime.now().year
        if None != request.POST.get("year", None):
            year = request.POST.get("year")

        year = str(year)

        data_summary = statistic.get_feeds_year_info_total(user_id=request.user.id, year=year)
        data_details = {
            "feeds" : statistic.get_feeds_year_info(user_id=request.user.id, year=year)
        }

    return json_response({
        "data_summary": data_summary,
        "data_details" : data_details
    })