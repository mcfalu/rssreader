from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.html import strip_tags

from reader.utils.json import json_response
from reader.utils.exeptions import AppException
from reader import models
from reader.views.feeds import get_menu_items
from reader.core.categories import get_categories
from reader.utils.request import get_param

#Page with lists of feeds
@login_required
def favorite(request):

    categories = get_categories(user_id=request.user.id)

    context = {
        'categories': categories,
        'menu_items' : get_menu_items(),
        'tags' : request.user.favorite_tags
    }

    return render(request, 'templates/tags/list.html', context)

# Get tags
@login_required
def items(request):

    if request.method == 'POST':

        tag = get_param(request, 'title', None)

        if None != tag and tag not in request.user.favorite_tags:

            request.user.favorite_tags.append(tag)
            request.user.save()

        success = True

        return json_response({'success' : success})

    elif request.method == 'GET':

        result = {
            'tags': request.user.favorite_tags
        }
        return json_response(result)
    else:
        return HttpResponse(content='Not allowed request method', status=405)

#Get post
@login_required
def item(request, tag):

    if request.method == 'DELETE':

        if tag in request.user.favorite_tags:

            request.user.favorite_tags.remove(tag)
            request.user.save()

        success = True

        return json_response({'success' : success})
    else:

        return HttpResponse(content='Not allowed request method', status=405)