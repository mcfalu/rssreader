import json

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.html import strip_tags
from django.http import Http404

from reader.utils.json import json_response
from reader.core.feeds import get_feeds, get_feed
from reader.core.categories import get_categories
from reader import models
from reader.forms.feed import FeedForm, FeedsFilterForm

#List of menu items in sidebar
def get_menu_items():

    result = [{
        'url' : '/feeds/',
        'current' :  "^/feeds/",
        'title' : 'Feeds'
    },{
        'url' : '/categories/',
        'current': "^/categories/",
        'title' : 'Categories'
    },{
        'url' : '/favorite-tags/',
        'current': "^/favorite-tags/",
        'title' : 'Favorite tags'
    }]

    return result

#Page with lists of feeds
@login_required
def feeds(request):

    form_config = {
        'category_options' : _get_user_category_options(request.user.id)
    }

    if request.method == 'POST':

        filter_form = FeedsFilterForm(form_config, request.POST)
        if filter_form.is_valid():

            request.session['feeds_filter'] = dict(category=filter_form.cleaned_data['category'])

    else:

        initial_data = dict()
        if ('feeds_filter' in request.session) and ('category' in request.session['feeds_filter']):
            initial_data['category'] = request.session['feeds_filter']['category']

        filter_form = FeedsFilterForm(form_config, initial=initial_data)

    category_id = None
    if ('feeds_filter' in request.session) and ('category' in request.session['feeds_filter']):
        category_id = request.session['feeds_filter']['category']

    feeds = get_feeds(user_id=request.user.id, category_id=category_id)

    context = {
        'filter_form' : filter_form,
        'feeds': feeds,
        'menu_items' : get_menu_items()
    }

    return render(request, 'templates/feeds/list.html', context)

# Get list of options for category input
def _get_user_category_options(uid):

    result = []
    result.append(('', '-'))

    categories = get_categories(user_id=uid, is_get_feeds=False)

    for cat_id in categories:

        cat_title = categories[cat_id]['title']

        result.append((cat_id, cat_title))

    return result

#Adding new feed
@login_required
def add(request):

    context = {
        'menu_items' : get_menu_items()
    }

    form_config = {
        'user_id' : request.user.id,
        'category_options' : _get_user_category_options(request.user.id)
    }

    if 'POST' == request.method:

        form = FeedForm(form_config, request.POST)

        if form.is_valid():

            title = form.cleaned_data['title'].strip()
            title = strip_tags(title)

            category_id = form.cleaned_data['category']

            #get category instance by it's id
            category = models.Categories.objects(id=category_id,
                                                 user_id=request.user.id).first()

            feed_instance = models.Feeds(title=title,
                                         user_id=request.user.id,
                                         site_url=form.cleaned_data['site_url'],
                                         link=form.cleaned_data['link'],
                                         category=category)

            feed_instance.save()

            return HttpResponseRedirect('/feeds/')

    else:
        form = FeedForm(form_config)

    context['form'] = form

    return render(request, 'templates/feeds/add.html', context)

#Editing existing feed
@login_required
def edit(request, feed_id):

    feed_info = get_feed(feed_id, request.user.id)
    if None == feed_info:
        raise Http404

    context = {
        'menu_items' : get_menu_items()
    }

    form_config = {
        'user_id' : request.user.id,
        'feed_id' : feed_id,
        'category_options' : _get_user_category_options(request.user.id)
    }

    if 'POST' == request.method:

        form = FeedForm(form_config, request.POST)
        if form.is_valid():

            title = form.cleaned_data['title']
            title = strip_tags(title)

            category_id = form.cleaned_data['category']

            feed_info.title = title
            feed_info.site_url = form.cleaned_data['site_url']
            feed_info.feed_link = form.cleaned_data['link']

            if category_id != feed_info.category.id:
                #get category instance by it's id
                category = models.Categories.objects(id=category_id,
                                                     user_id=request.user.id).first()

                feed_info.category = category

            feed_info.save()

            return HttpResponseRedirect('/feeds/')
    else:

        default_data=dict(title=feed_info.title,
                          site_url=feed_info.site_url,
                          category=feed_info.category.id,
                          link=feed_info.link)

        form = FeedForm(form_config, initial=default_data)

    context['form'] = form

    return render(request, 'templates/feeds/edit.html', context)

#Delete existing feed
@login_required
def delete(request, feed_id):

    feed_info = get_feed(feed_id, request.user.id)
    if None == feed_info:
        raise Http404

    try:

        feed_info.delete()

        return json_response({
            "success": True,
        })

    except Exception, e:

        return json_response({
            "success": False,
        })