from django.shortcuts import render
from django.contrib import auth
from django.core.context_processors import csrf
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from reader.forms.account import AccountForm, ChangePasswordForm, ChangeUsernameForm

#List of menu items in sidebar
def get_menu_items():

    result = [{
        'url' : '/profile/',
        'current' :  '^/profile/',
        'title' : 'Account data'
    },
    {
        'url' : '/change-password/',
        'current' :  '^/change-password/',
        'title' : 'Change password'
    },
    {
        'url' : '/change-username/',
        'current' :  '^/change-username/',
        'title' : 'Change username'
    }]

    return result

# Login form
def login(request):

    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    context = {}
    context.update(csrf(request))

    if request.method == 'POST':

        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = auth.authenticate(username=username, password=password)

        if user is not None:

            auth.login(request, user)
            return HttpResponseRedirect('/')

        else:

            context['form_error'] = "Wrong login or password"

    return render(request, 'templates/account/login.html', context)


# Log out action
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


@login_required
def profile(request):

    context = {
        "menu_items" : get_menu_items()
    }

    form_config = {
        'user' : request.user
    }

    if 'POST' == request.method:

        form = AccountForm(form_config, request.POST)

        if form.is_valid():

            try:

                request.user.email = form.cleaned_data['email'].strip()
                request.user.first_name = form.cleaned_data['first_name'].strip()
                request.user.last_name = form.cleaned_data['last_name'].strip()
                request.user.save()

                context["form_message"] = "Account info has been successfully updated"

            except Exception, e:

                context["form_error"] = "Error when trying to update account"
    else:

        form = AccountForm(form_config, initial=dict(username=request.user.username,
                                        email=request.user.email,
                                        first_name=request.user.first_name,
                                        last_name=request.user.last_name))

    context["form"] = form

    return render(request, 'templates/account/profile.html', context)


def change_password(request):

    context = {
        "menu_items" : get_menu_items()
    }

    form_config = {
        'user' : request.user
    }

    if 'POST' == request.method:

        form = ChangePasswordForm(form_config, request.POST)

        if form.is_valid():

            try:

                new_password = form.cleaned_data['new_password'].strip()
                request.user.set_password(new_password)

                context["form_message"] = "Password has been successfully updated"

            except Exception, e:

                context["form_error"] = "Error when trying to update account"
    else:

        form = ChangePasswordForm(form_config)

    context["form"] = form

    return render(request, 'templates/account/change_password.html', context)


def change_username(request):

    context = {
        "menu_items" : get_menu_items()
    }

    form_config = {
        'user' : request.user
    }

    if 'POST' == request.method:

        form = ChangeUsernameForm(form_config, request.POST)
        if form.is_valid():

            try:

                request.user.username = form.cleaned_data['username']
                request.user.save()

                context["form_message"] = "Username has been successfully updated"

            except Exception, e:

                context["form_error"] = e#"Error when trying to update account"

    else:

        form = ChangeUsernameForm(form_config, initial=dict(username=request.user.username))

    context["form"] = form

    return render(request, 'templates/account/change_username.html', context)