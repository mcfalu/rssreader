from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings

from reader.utils.json import json_response
from reader.core.categories import get_categories
from reader.core import posts


# Show all posts from active feeds
@login_required
def index(request):

    search_string = request.GET.get('q', '')

    context = get_feed_content(limit=settings.LIMIT_POSTS,
                               is_viewed=0,
                               user_id=request.user.id,
                               search_string=search_string)

    context['search_string'] = search_string

    return render(request, 'templates/index.html', context)

# Get content data for feed
def get_feed_content(feed_id=None, category_id=None, limit=30, offset=0, is_viewed=None, user_id=None, search_string='', is_important=None):

    posts_items = posts.get_posts(limit=limit,
                                  offset=offset,
                                  feed_id=feed_id,
                                  is_viewed=is_viewed,
                                  user_id=user_id,
                                  search_string=search_string,
                                  is_important=is_important)

    first_post_info = {
        "id": ""
    }

    if 0 != len(posts_items):
        first_post_info['id'] = posts.get_last_id()

    # Format posts
    posts_items = posts.format_posts(posts_items)
    categories = get_categories(user_id)

    total_unviewed = 0
    for i in categories:
        total_unviewed += categories[i]['count_unviewed']

    result = {
        'current_feed_id': feed_id,
        'current_category_id': category_id,
        'feed': {
            'offset': limit,
            'limit': limit,
            'posts': posts_items
        },
        'first_post_info': first_post_info,
        'categories': categories,
        'total_important': posts.get_count_important_posts(is_viewed=0),
        'total_unviewed': total_unviewed
    }

    return result