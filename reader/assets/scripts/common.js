require(['jquery'], function($) {

    $(function(){

        $('.btn-navbar').click(function(){

            $('#sidebar-wrapp').toggleClass('in');

            if ($('#sidebar-wrapp').hasClass('in')){

                $('.row-fluid .span3').width('250px');

            }else{

                $('.row-fluid .span3').width('0');

            }

            return false;
        });

        $('.feed-search-bar form').submit(function(){

            if (0==$('.feed-wrapp').first().length)
            {
                var queryString = $('.feed-search-bar #search-input').val();
                location.href =  '/?q='+queryString;
                return false;
            }
        });
    });
});