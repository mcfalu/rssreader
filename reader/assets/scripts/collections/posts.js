define(['backbone' , 'models/post'], function(Backbone, PostModel){

    var PostsCollection = Backbone.Collection.extend({
        initialize: function() {
            this._attributes = {}
        },
        model: PostModel,
        url: function() {
            return '/posts/';
        },
        parse: function(data){

            var attrs = ['last_post_id', 'next_offset', 'count_unviewed_posts'];
            for (i in attrs){

                var attr = attrs[i];
                if (undefined != data[attr]){
                    this.attr(attr, data[attr]);
                }
            }

            return data.posts;
        },
        attr: function(prop, value) {

            if (value === undefined) {
                return this._attributes[prop]
            } else {
                this._attributes[prop] = value;
                this.trigger('change:' + prop, value);
            }
        },
    });

    return PostsCollection;
});