require(['jquery', 'modal-dialog'], function($, modalDialog) {

    $('.feed-remove').click(function(){

        var row = $(this).parents('tr').first();
        var title = $(this).parent().find('.feed-info').attr('data-title');
        var id = $(this).parent().find('.feed-info').attr('data-id')

        modalDialog.render({
            "title": "Remove feed",
            "content" : "Are you sure want to remove feed: <strong>'" + title + "'</strong>",
            "buttons" : {
                "submit" : {
                    "title": "Remove",
                    "onClick" : function(el, modalWindow, dialog){

                        $.ajax({
                            type: "GET",
                            dataType: 'json',
                            url: '/feeds/delete/'+id+'/',
                            success: function(res){

                                if (undefined!=res.success && true==res.success){
                                    row.remove();
                                    modalWindow.modal('hide');
                                }else{
                                    var message = 'Error when trying to remove feed';
                                    dialog.showError(message);
                                }
                            },
                            error: function(res){

                                var message = 'Error when trying to remove feed';
                                dialog.showError(message);
                            }
                        });
                    }
                },
            }
        });
    });
});