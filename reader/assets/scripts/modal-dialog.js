define(['text!templates/modal/dialog.html'], function(dialogTemplate ) {

    var ModalDialog = function() {

        var defaultOptions = {
            "wrapper" : "#modal-wrapp",
            "title" : "",
            "content" : "",
            "afterRender" : function(){
            },
            "buttons" : {
                "submit" : {
                    "title" : "Ok",
                    "onClick" : null
                },
                "cancel" : {
                    "title" : "Cancel",
                    "class" : "close",
                    "onClick" : function(el){
                        $(el).parents('.modal').find('.close').click();
                    }
                }
            }
        }

        var options = defaultOptions;

        this.render = function(data, wrapper){

            var self = this;

            options = $.extend({}, defaultOptions, data);
            options.buttons = $.extend({}, defaultOptions.buttons, data.buttons);

            var t = _.template(dialogTemplate, {
                "title" : options.title,
                "content" : options.content,
                "buttons" : {
                    "submit" : {
                        "title" : options.buttons.submit.title
                    },
                    "cancel" : {
                        "title" : options.buttons.cancel.title,
                    }
                }
            });

            if (undefined==$(options.wrapper).html()){

                if ('#' == defaultOptions.wrapper.substring(0,1)){

                    var elName = defaultOptions.wrapper.substring(1);
                    $('body').append('<div id="'+elName+'"></div>');

                } else {
                    if ('.' == defaultOptions.wrapper.substring(0,1)){
                        var elName = defaultOptions.wrapper.substring(1);
                        $('body').append('<div class="'+elName+'"></div>');
                    }
                }
            }

            $(options.wrapper).html(t);

            if ('function' == typeof options.afterRender){

                var modal = $(options.wrapper + ' .modal');
                options.afterRender(modal);
            }

            $(options.wrapper + ' .modal').modal().on('shown', function () {

                var modal = $(options.wrapper + ' .modal');

                /*on click submit button */
                if ('function' == typeof options.buttons.submit.onClick){

                    $(options.wrapper + ' .modal .btn-submit').click(function(e){

                        options.buttons.submit.onClick(e.target, modal, self);
                    });
                }

                /*on click cancel button */
                if ('function' == typeof options.buttons.cancel.onClick){

                    $(options.wrapper + ' .modal .btn-cancel').click(function(e){

                        options.buttons.cancel.onClick(e.target, modal, self);
                    });
                }

            }).on('hidden', function () {
                $(options.wrapper).remove();
            });
        }

        this.showError = function(message){

            if (0!=$(options.wrapper+' .modal-body').find('.alert-error').length){
                $(options.wrapper+' .modal-body .alert-error').html(message);
            }else{
                var errorMessage = '<div class="alert alert-error">';
                errorMessage += message;
                errorMessage += '</div>';
                $(options.wrapper+' .modal-body').prepend(errorMessage);
            }
        }
    }

    return new ModalDialog();
});