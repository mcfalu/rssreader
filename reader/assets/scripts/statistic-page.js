require(['jquery', 'chart'], function($, Chart) {

    var myLineChart = null;
    var dataType = 'month';

    var Chartjs = Chart.noConflict();
    Chart.defaults.global.animation = false;
    Chart.defaults.global.tooltipFontSize = 12;
    Chart.defaults.global.tooltipCornerRadius = 0;
    Chart.defaults.global.tooltipXPadding = 4;
    Chart.defaults.global.tooltipYPadding = 4;
    Chart.defaults.global.tooltipCaretSize = 4;
    Chart.defaults.global.tooltipTemplate = "<%= value %>";

    var defaultChartData = {
        labels: [],
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: []
            }
        ]
    };

    var renderStatistic = function(data){

        var chartData = defaultChartData;
        for (item in data){

            chartData.labels.push(data[item].title)
            chartData.datasets[0].data.push(data[item].value);
        }

        var ctx = $("#chart-div")[0].getContext("2d");

        myLineChart = new Chart(ctx).Line(chartData, {
            pointDot : false,
            bezierCurve: false,
            pointHitDetectionRadius: 8
        });

        chartData.labels = [];
        chartData.datasets[0].data = [];
    }

    var updateStatistic = function(type){

        if (null!=myLineChart){
            myLineChart.destroy();
        }
        $('#table-statistic-details').html('');

        var data = {};
        if ('month'==type){

            data.month = $('#stat_mont_filter #id_month').val();
            data.year = $('#stat_mont_filter #id_year').val();

            $('.control-month').show();
        }else{

            if ('year'==type){
                data.year = $('#stat_mont_filter #id_year').val();
                $('.control-month').hide();
            }
        }

        $('.chart-wrapp .ajax-loader').show();

        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: '/statistic/data/'+type+'/',
            statusCode: {

                200: function(res) {

                    $('.chart-wrapp .ajax-loader').hide();

                    if (res.data_summary){

                        var total = 0;
                        _.each(res.data_summary, function(itemInfo){
                            total += itemInfo.value;
                        });

                        renderStatistic(res.data_summary);

                        require(['text!templates/statistic/feeds-details.html'], function(template) {

                            var t = _.template(template, {
                                feeds : res.data_details.feeds,
                                total: total
                            });

                            $('#table-statistic-details').html(t);

                        });
                    }
                }
            }
        });
    }

    $('.nav-tabs a').click(function(){

        $('.nav-tabs li').removeClass('active');
        $(this).parent().addClass('active');

        dataType = $(this).attr('data-type');
        updateStatistic(dataType);
    });

    $('#stat_mont_filter select').change(function(){
        updateStatistic(dataType);
    });

    updateStatistic(dataType);
});