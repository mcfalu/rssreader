require.config({
    baseUrl : '/static/scripts',
    paths: {
        jquery: '/static/scripts/lib/jquery-1.10.2.min',
        underscore: '/static/scripts/lib/underscore',
        backbone: '/static/scripts/lib/backbone-min',
        text: '/static/scripts/lib/text',
        bootstrap: '/static/bootstrap/js/bootstrap.min',
        jqueryValidation: '/static/scripts/lib/jquery.validate.min',
        chart: '/static/scripts/lib/Chart.min'
    },
    shim: {
        "underscore": {
            deps: [],
            exports: "_"
        },
        "backbone": {
            deps: ["jquery", "underscore"],
            exports: "Backbone"
        },
    }
});

require(['jquery', 'underscore', 'backbone'], function($, _, Backbone){

    require(['bootstrap', 'common']);

    var csrfToken = $('body').attr('data-csrf-token');
    $.ajaxSetup({
        headers: { "X-CSRFToken": csrfToken }
    });
});