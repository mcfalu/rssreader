define(['collections/posts', 'models/post'], function(PostsCollection, PostModel) {

    var Feed = function(){

        var feedOptions = {
            is_important: '',
            is_viewed : 0,
            feed_id : '',
            category_id : '',
            limit : 30,
            offset : 0,
            read_later : '',
            search: '',
            last_post_id : 0
        }

        var isAllowedRequest = true;

        this.setOptions = function(data){

            if (undefined==data)
                return null;

            for (k in data) {
                this.option(k, data[k])
            }
        }

        this.option = function(option, value){

            if (undefined==option){
                return null;
            }

            if (undefined==value){

                if (undefined!=feedOptions[option]){
                    return feedOptions[option];
                }
            }else{

                if (undefined!=feedOptions[option]){
                    feedOptions[option] = value;
                }
            }
        }

        this.loadPosts = function(options){

            if (!isAllowedRequest){
                return false;
            }

            var self = this;

            self.ajaxLoader('show');

            var defaultOptions = {
                "beforeRender" : function(){},
                "afterRender" : function(){}
            }

            var options = $.extend(defaultOptions, options);
            var params = {};

            if (''!==feedOptions.is_viewed){
                params['is_viewed'] = feedOptions.is_viewed;
            }

            if (''!==feedOptions.limit){
                params['limit'] = feedOptions.limit;
            }

            if (''!=feedOptions.feed_id && 'None'!=feedOptions.feed_id){
                params['feed_id'] = feedOptions.feed_id;
            }

            if (''!=feedOptions.category_id && 'None'!=feedOptions.category_id){
                params['category_id'] = feedOptions.category_id;
            }

            if (''!=feedOptions.offset){
                params['offset'] = feedOptions.offset;
            }

            if (''!=feedOptions.read_later){
                params['read_later'] = feedOptions.read_later;
            }

            if (''!=feedOptions.search){
                params['search'] = feedOptions.search;
            }

            if (''!==feedOptions.is_important){
                params['is_important'] = feedOptions.is_important;
            }

            isAllowedRequest = false;

            var posts = new PostsCollection();
            posts.fetch({
                data: params,
                success: function(postCollection){

                    var countPosts = postCollection.length;

                    self.renderPosts({
                        'posts' : postCollection,
                        'itemRender' : function(html){
                            $('#load-more-posts-sep').before(html);
                        },
                        'beforeRender': function(){

                            if ('function'== typeof options.beforeRender){
                                options.beforeRender(postCollection);
                            }
                        },
                        'afterRender' :function(){

                            self.option('last_post_id', postCollection.attr('last_post_id'));
                            self.option('offset', postCollection.attr('next_offset'));

                            if (countPosts<feedOptions.limit){

                                $('#load-more-posts').hide();

                            }else{

                                $('#load-more-posts').show();
                            }

                            if ('function'== typeof options.afterRender){
                                options.afterRender(postCollection);
                            }

                            if (''!=feedOptions.feed_id){

                                $('.category-opt').hide();
                                $('.feed-opt').show();

                            } else {

                                if (''!=feedOptions.category_id){

                                    $('.feed-opt').hide();
                                    $('.category-opt').show();

                                } else {

                                    $('.feed-opt').hide();
                                    $('.category-opt').hide();
                                }
                            }

                            self.ajaxLoader('hide');
                        }
                    });

                    isAllowedRequest = true;
                }
            });
        }

        this.update = function(onSuccess){

            var feed = this;

            var feedId = feed.option('feed_id');
            var categoryId = feed.option('category_id');
            var lastPostId = feed.option('last_post_id');
            var isViewed = feed.option('is_viewed');

            var params = {
                last_post_id: lastPostId,
                force_update: 1
            };

            if (''!=feedId && 'None'!=feedId){
                params['feed_id'] = feedId;
            }

            if (''!= categoryId && 'None'!=categoryId){
                params['category_id'] = categoryId;
            }

            if (''!=isViewed){
                params['is_viewed'] = isViewed;
            }

            feed.ajaxLoader('show');

            var posts = new PostsCollection();
            posts.fetch({
                data: params,
                success: function(postCollection){

                    feed.renderPosts({
                        "posts" : postCollection,
                        "itemRender" : function(html){
                            $('#new-posts-sep').before(html);
                        },
                        "afterRender" :function(){

                            feed.option('last_post_id', postCollection.attr('last_post_id'));

                            if (0!=postCollection.length){

                                $('.no-feeds').hide();
                            }

                            var countUnviewedPosts = postCollection.attr('count_unviewed_posts');

                            if (undefined!=countUnviewedPosts){

                                for(i in countUnviewedPosts){

                                    var item = countUnviewedPosts[i];

                                    if (undefined!=item['feed_id']
                                        && undefined!=item['count']){

                                        feed.setCountViewed(item['feed_id'], 'set', item['count']);
                                    }
                                }
                            }

                            if (undefined != onSuccess && typeof onSuccess == 'function'){

                                onSuccess();
                            }

                            feed.ajaxLoader('hide');
                        }
                    });
                }
            });
        }

        this.renderPosts = function(options){

            var defaultOptions = {
                "posts" : {},
                "beforeRender" : function(){},
                "afterRender" : function(){},
                "itemRender" : function(){}
            }

            var options = $.extend(defaultOptions, options);
            var countPosts = options.posts.length;

            require(['text!templates/feed/item.html'], function(feedItemTemplate) {

                if ('function'== typeof options.beforeRender){
                    options.beforeRender();
                }

                options.posts.each(function(post){

                    var htmlItem = _.template(feedItemTemplate, {
                        'post_id' : post.get('id'),
                        'category_id' : post.get('category_id'),
                        'feed_title' : post.get('feed_title'),
                        'date_created' : post.get('date_created'),
                        'title' : post.get('title'),
                        'link' : post.get('link'),
                        'post_image' : post.get('image'),
                        'tags' : post.get('tags'),
                        'is_viewed' : post.get('is_viewed'),
                        'feed_id' : post.get('feed_id'),
                        'read_later' : post.get('read_later')
                    });

                    if ('function'== typeof options.itemRender){
                        options.itemRender(htmlItem);
                    }

                });

                if ('function'== typeof options.afterRender){
                    options.afterRender();
                }

                if (0!=options.posts.length){

                    $('.feed-load-more .ajax-loader').hide();
                    $('.feed-load-more a').show();
                }

            });
        }

        this.setCountViewed = function(id, mode, value){

            var self = this;
            var selector = 'a.feed-link';

            if (undefined!=id){

                selector += '[data-id="'+id+'"]';
                selector += ' em';

                $(selector).each(function(){

                    var count = $(this).html();

                    if (''!=count){

                        var count = parseInt(count);
                        var prevValue = count;

                        if (undefined!=value){
                            count = value;
                        }

                        var catEl = $(this).parents('.sel-item').find('a.category-link .count-unviewed em');
                        var catCount = parseInt(catEl.html());

                        if (isNaN(catCount)){
                            catCount = 0;
                        }

                        if (undefined!=mode){

                            if ('inc'==mode){
                                count++;
                                catCount++;
                            }

                            if ('dec'==mode){
                                count--;
                                catCount--;
                            }

                            if ('set'==mode){
                                catCount -= prevValue;
                                catCount += value;
                            }
                        }

                        if (catCount<0){
                            catCount = 0;
                        }

                        $(this).html(count);
                        catEl.html(catCount);
                    }
                });

                self.refreshTotalUnviewed();
            }
        }

        this.refreshTotalUnviewed = function(){

            var totalUnviewed = 0;
            $('.category-link .count-unviewed em').each(function(){

                var v = $(this).html().trim();
                totalUnviewed += parseInt(v);
            });

            $('#all-posts .count-unviewed em').html(totalUnviewed);

            this.refreshUnviwedNotify();
        }

        this.refreshUnviwedNotify = function(){

            $('.count-unviewed').each(function(){

                if (0!=$(this).find('em').html()){
                    $(this).show();
                } else{
                    $(this).hide();
                }
            });
        }

        this.markPostAsRead = function(postId, onSuccess){

            var self = this;
            self.getPostInfo(postId, function(post){

                post.save({
                    is_viewed: 1
                },{
                    patch : true,
                    success :function(post){

                        if ('function'== typeof onSuccess){
                            onSuccess(post);
                        }

                        self.setCountViewed(post.get('feed_id'), 'dec');

                        if (1==post.get('is_important')){

                            var countImortantPosts = self.getCountImportantPosts();
                            if (countImortantPosts>0){
                                countImortantPosts--;
                                self.setCountImportantPosts(countImortantPosts);
                            }
                        }
                    }
                });
            });
        }

        this.markPostsAsRead = function(options){

            var self = this;

            var defaultOptions = {
                "category_id" : "",
                "feed_id" : "",
                "is_important": "",
                "onSuccess" : function(){}
            }

            var options = $.extend(defaultOptions, options);

            //if post id is defined - we need to mark only this post as read
            if ('None'==options.category_id){
                options.category_id = '';
            }
            if ('None'==options.feed_id){
                options.feed_id = '';
            }
            if ('None'==options.is_important){
                options.is_important = '';
            }

            var data = {};

            if ('' != options.feed_id){
                data.feed_id = options.feed_id;
            }else{

                if ('' != options.category_id){
                    data.category_id = options.category_id;
                }
            }

            if ('' != options.is_important){
                data.is_important = options.is_important;
            }

            var onSuccess = function(){
                $('.feed-post-item').removeClass('feed-post-unviewed');
            }

            onSuccess = function(response){

                if (response.count_unviewed_posts){

                    var countUnviewedPosts = response.count_unviewed_posts;

                    if (undefined!=countUnviewedPosts){

                        var feedsInfo = {};

                        if (0!=countUnviewedPosts.length){

                            for(i in countUnviewedPosts){

                                var item = countUnviewedPosts[i];

                                if (undefined!=item['feed_id']
                                    && undefined!=item['count']){

                                    feedsInfo[item['feed_id']] = item['count'];
                                }
                            }
                        }

                        if (response.total_important){
                            self.setCountImportantPosts(response.total_important);
                        }

                        $('a.feed-link').each(function(){

                            var feedId = $(this).attr('data-id');
                            var countUnviewed = 0;
                            if (feedsInfo[feedId]){
                                countUnviewed = feedsInfo[feedId];
                            }

                            self.setCountViewed(feedId, 'set', countUnviewed);
                        });
                    }

                    $('.feed-post-item').removeClass('feed-post-unviewed');
                    $('#load-more-posts a').hide();
                }
            }

            $.ajax({
                type: "POST",
                data : data,
                dataType: 'json',
                url: '/posts-mark-as-read/',
                statusCode: {

                    200: function(res) {

                        if (res.success && true==res.success){

                            onSuccess(res);

                            if ('function'== typeof options.onSuccess){
                                options.onSuccess(res);
                            }
                        }
                    }
                }
            });
        }

        this.getPostInfo = function(postId, onSuccess){

            var post = new PostModel({
                id : postId
            });
            post.fetch({
                'success': function(post){

                    if (undefined!=onSuccess && 'function'== typeof onSuccess){

                        onSuccess(post);
                    }
                },
                'error': function(){
                }
            });
        }

        this.ajaxLoader = function(mode){

            if ('show'==mode){

                $('.feed-refresh').show();
            }

            if ('hide'==mode){

                $('.feed-refresh').hide();
            }
        }

        this.readItLater = function(postId, value, onSuccess){

            var post = new PostModel({
                id : postId
            });
            post.save({
                read_later: value
            },{
                patch : true,
                success :function(post){

                    if ('function'== typeof onSuccess){
                        onSuccess(post);
                    }
                }
            });
        }

        this.getCountImportantPosts = function(){

            var countViewed = $('#important-posts .count-unviewed em').html().trim();
            countViewed = parseInt(countViewed);
            if (countViewed>0){
                return countViewed;
            }
            return 0;
        }

        this.setCountImportantPosts = function(value){

            if (!isNaN(value)){

                if(value <= 0){
                    value = '';
                }
                $('#important-posts .count-unviewed em').html(value);
            }
        }

        this.showFullPost = function(el, post){

            require(['text!templates/feed/item-details.html'], function(postTemplate) {

                var t = _.template(postTemplate, {
                    summary : post.get('summary'),
                    tags : post.get('tags'),
                    image : post.get('image')
                });

                $('.feed-post-summary').removeClass('feed-post-summary');

                $(el).removeClass('feed-post-unviewed');
                $(el).find('.details').html(t);
                $(el).addClass('feed-post-summary');

                var top = $(el).offset().top;
                var toolbarHeight = parseInt($('.feed-toolbar').first().height()) ;
                var menuBar = parseInt($('.menu-bar').first().height());

                $(window).scrollTop(top - (toolbarHeight + menuBar + 15));

                $(el).focus();

            });
        }

        this.getCountPostsInFeed = function(){

            return $('.feed-post-item').length;
        }

        this.getActivePostIndex = function(){

            var index = $('.feed-post-summary').first().index();
            if (-1 != index){
                return index - 1;
            }
            return -1;
        }
    }

    return new Feed();
})