require(['jquery', 'text!templates/tags/tag.html', 'models/tag'],

    function($, tagTemplate, TagModel) {

        var existingTags = [];
        $('.fav-tags .tag span').each(function(){
            existingTags.push($(this).html().trim());
        });

        var addTag = function(){

            var title = $('#tag-title').val().trim();

            if (''!=title){

                if (-1==$.inArray(title, existingTags)){

                    var t = _.template(tagTemplate, {
                        title : title
                    });

                    $('#fav-tag').append(t);

                    var tag = new TagModel();
                    tag.save({
                        title: title
                    }, {
                        'success': function(){
                        },
                        'error': function(){
                        }
                    });
                }

                $('#tag-title').val('');
            }
        }

        $('#form-new-tag').submit(function(e){
            addTag();
            return false;
        });

        $('#btn-add-tag').click(function(){
            addTag();
        });

        $('.fav-tags').on('click', '.tag em', function(){

            var tagDiv = $(this).parent();
            var tagTitle = $(this).parent().find('span').html();
            var tag = new TagModel({
                id: tagTitle
            });
            tag.destroy({
                'success': function(){
                    tagDiv.remove();
                }
            });

            return false;
        });
});