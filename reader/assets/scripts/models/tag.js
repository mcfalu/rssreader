define(['backbone'], function(Backbone) {

    var Tag = Backbone.Model.extend({
        defaults: {
            id: null,
            title: ''
        },
        url: function() {
            return this.id ? '/profile/tags/' + this.id  : '/profile/tags/';
        }
    });

    return Tag;
});