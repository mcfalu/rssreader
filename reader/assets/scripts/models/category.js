efine(['Backbone'], function() {

    var Category = Backbone.Model.extend({
        defaults: {
            id: null,
            title: ""
        },
        url: function() {
            return this.id ? '/category/' + this.id + '/' : '/category/';
        }
    });

    return Category;
});