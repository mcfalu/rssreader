define(['backbone'], function(Backbone) {

    var Post = Backbone.Model.extend({
        defaults: {
            id: null,
            category_id: null,
            title: "",
            link : "",
            tags: [],
            image: "",
            read_later: 0,
            is_viewed: 0,
            is_important: 0,
            date_created: "",
            date_added: "",
            feed_id:  null,
            feed_title: ""
        },
        url: function() {
            return this.id ? '/posts/' + this.id  : '/posts/';
        }
    });

    return Post;
});