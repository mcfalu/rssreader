require(['jquery', 'app', 'modal-dialog'], function( $, app, modalDialog) {

    $(function(){

        app.feed.option('last_post_id', $('.btn-refresh a').attr('data-first-post-id'));

        function getDocHeight() {
            var D = document;
            return Math.max(
                Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
                Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
                Math.max(D.body.clientHeight, D.documentElement.clientHeight)
            );
        }

        $(window).scrollTop(0);

        $(window).scroll(function() {

            if($(window).scrollTop() + $(window).height()  == getDocHeight() ) {

                if ($('#load-more-posts a').is(":visible")){

                    $('#load-more-posts a').click();
                }
            }
        });

        /** Load more posts **/
        $('#load-more-posts a').click(function(){

            $('#load-more-posts').hide();

            app.feed.loadPosts();

            return false;
        });

        $('.btn-refresh a').click(function(){

            app.feed.update(function(){
                app.updateWindowTitle();
            });

            return false;
        });


        /** Marked as read post **/
        $('.feed-wrapp').on('click', '.feed-post-unviewed', function(){

            var self = this;
            var postId = $(this).find('a.post-link').attr('data-id');

            app.feed.markPostAsRead(postId, function(post){

                app.updateWindowTitle();

                app.feed.showFullPost(self, post);
            });

        });


        /** Show summary of post **/
        $('.feed-wrapp').on('click', '.feed-post-item', function(){

            if (!$(this).hasClass('feed-post-unviewed') && !$(this).hasClass()){

                var self = this;

                var postId = $(this).find('.post-link').attr('data-id');
                app.feed.getPostInfo(postId, function(post){

                    app.feed.showFullPost(self, post);
                });
            }

        });


        /** Changing type of viewed  **/
        $('#feed-is-viewed a').click(function(){

            $('#feed-is-viewed a').removeClass('active');
            $(this).addClass('active');

            var isViewed = $(this).attr('data-value');

            $('#feed-is-viewed').attr('data-selected-value', isViewed);

            app.feed.setOptions({
                'is_viewed' : isViewed,
                'offset' : 0
            });

            app.feed.loadPosts({
                "beforeRender": function() {
                    $('.feed-post-item').remove();
                }
            })

            return false;
        });


        /** Changing feed **/
        $('#nav-categories').on('click','.feed-link', function(){

            var self = this;
            var feedId = $(self).attr('data-id');

            var feedTitle = $(self).find('.feed-title').html().trim();
            var feedCountUnviewed = $(self).find('.count-unviewed em').html().trim();

            //mark as active current feed
            $('.sidebar-nav .nav-list li ul li').removeClass('active');
            $(self).parents('li').first().addClass('active');

            app.feed.setOptions({
                'read_later' : '',
                'feed_id' : feedId,
                'offset' : 0
            });

            //reload feed with posts
            app.feed.loadPosts({
                "beforeRender": function() {
                    $('.feed-post-item').remove();
                },
                "afterRender": function(){
                    app.updateWindowTitle();
                }
            });
        });


        /** Mark all posts as read **/
        $('.mark-all-as-read a.btn-mark').click(function(){

            var markPostsAsRead = function(){

                app.feed.markPostsAsRead({
                    "feed_id" : app.feed.option('feed_id'),
                    "is_important" : app.feed.option('is_important'),
                    "category_id" : app.feed.option('category_id'),
                    "onSuccess" : function(){
                        app.updateWindowTitle();
                    }
                });
            }

            require(['modal-dialog'], function(modalDialog) {

                modalDialog.render({
                    "title": "Mark as read",
                    "content" : "Are you sure want to mark all items as read",
                    "buttons" : {
                        "submit" : {
                            "title": "Confirm",
                            "onClick" : function(el, modalWindow){

                                markPostsAsRead();

                                modalWindow.modal('hide');
                            }
                        },
                    }
                });
            });

            return false;
        });

        /** Show summary of post **/
        $('.feed-wrapp').on('click', '.read-it-later', function(){

            var self = this;
            var val = 0;
            if ($(this).find('em').hasClass('icon-star-empty')){
                val = 1;
            }

            var postId = $(this).parents('.feed-post-item').first().find('.post-link').attr('data-id');

            app.feed.readItLater(postId, val, function(){

                $(self).find('em').toggleClass('icon-star').toggleClass('icon-star-empty');
            });

            return false;
        });


        /** On change sidebar menu item **/
        $('.sidebar-nav').on('click', 'li.sel-item > a', function(){

            var self = this;

            $('.sidebar-nav .nav-list li').removeClass('active');
            $(self).parents('li').first().addClass('active');

            return false;
        });


        /** On change category item **/
        $('#nav-categories').on('click', 'li a.category-link', function(){

            var categoryId = $(this).attr('data-id');
            app.openCategory(categoryId);

            $('.feed-toolbar .btn').show();
        });

        $('#all-posts').click(function(){

            $('.feed-toolbar .btn').show();

            app.openCommonCategory({
                'is_important' : '',
                'feed_id' : '',
                'category_id' : '',
                'offset' : 0,
                'read_later' : ''
            });
        });

        $('#saved-posts').click(function(){

            $('.feed-toolbar .btn').hide();

            app.openCommonCategory({
                'is_important' : '',
                'read_later' : 1,
                'feed_id' : '',
                'category_id' : '',
                'offset' : 0
            });
        });

        $('#important-posts').click(function(){

            $('.feed-toolbar .btn').show();

            app.openCommonCategory({
                'is_important' : '1',
                'read_later' : '',
                'feed_id' : '',
                'category_id' : '',
                'offset' : 0
            });
        });

        $('.feed-search-bar form').submit(function(){

            var queryString = $('.feed-search-bar #search-input').val();

            app.feed.setOptions({
                'offset' : 0,
                'search' : queryString
            });

            $('#load-more-posts').hide();
            $('.feed-post-item').remove();

            app.feed.loadPosts({
                "afterRender": function(){
                    app.updateWindowTitle();
                }
            });

            return false;
        });

        $('.feed-wrapp').on('click', '.tags a', function(){

            $(window).scrollTop(0);
            var s = 'tag:'+$(this).html();

            $('.feed-search-bar #search-input').val(s);
            $('.feed-search-bar form').submit();

            return false;
        });

        $(document.documentElement).keyup(function (e) {

            if (e.keyCode == 39){

                var activePostIndex = app.feed.getActivePostIndex();

                $('.feed-wrapp .feed-post-item').eq(activePostIndex + 1).click();
            }
            if (e.keyCode == 37){

                var activePostIndex = app.feed.getActivePostIndex();
                if (activePostIndex >= 1){

                    $('.feed-wrapp .feed-post-item').eq(activePostIndex - 1).click();
                }
            }

        });

    });
});