define(['feed'], function(feed) {

    var App = function(){

        this.feed = feed;

        this.feed.option('offset', $('#feed-is-viewed').attr('data-offset'));

        this.openCommonCategory = function(options){

            var app = this;

            app.closeCategories();
            app.feed.setOptions(options);
            app.feed.loadPosts({
                "beforeRender": function() {
                    $('.feed-post-item').remove();
                },
                "afterRender": function(){
                    app.updateWindowTitle();
                }
            });
        }

        this.openCategory = function(id){

            var app = this;

            app.feed.setOptions({
                'is_important' : '',
                'feed_id' : '',
                'category_id' : id,
                'offset' : 0,
                'read_later' : ''
            });

            app.feed.loadPosts({
                "beforeRender": function() {
                    $('.feed-post-item').remove();
                },
                "afterRender": function(){
                    app.updateWindowTitle();
                }
            });

            app.closeCategories();

            var a = $('#nav-categories li a.category-link[data-id="'+id+'"]');
            a.find('i').removeClass('icon-folder-close').addClass('icon-folder-open');
        }

        this.closeCategories = function(){

            $('#nav-categories li a').find('i').removeClass('icon-folder-open').addClass('icon-folder-close');
        }

        this.updateWindowTitle = function(){

            var title = 'All posts';
            var countUnviewed = this.getCountUnviewed();

            var categoryId = this.feed.option('category_id');
            var categoryTitle = this.getCategoryTitle(categoryId);

            var feedId = this.feed.option('feed_id');
            var feedTitle = this.getFeedId(feedId);

            if (''!=feedId){
                title = feedTitle;
                countUnviewed = this.getCountUnviewed(feedId, 'feed');
            }else{

                if (''!=categoryId){

                    title = categoryTitle;
                    countUnviewed = this.getCountUnviewed(categoryId, 'category');

                }else{

                    if (1==this.feed.option('read_later')){
                        title = 'Saved';
                    }
                }
            }

            if (undefined !== countUnviewed && 0 != countUnviewed){
                title = ' (' + countUnviewed + ') ' + title;
            }

            document.title = title;

        }

        this.getCategoryTitle = function(id){

            if (''!=id){

                var a = $('#nav-categories li a.category-link[data-id="'+id+'"]');
                if (undefined!=a){
                    return a.find('.category-title').html().trim();
                }
            }

            return '';
        }

        this.getFeedId = function(id){

            if (''!=id){

                var a = $('#nav-categories li a.feed-link[data-id="'+id+'"]');
                if (undefined!=a){
                    return a.find('.feed-title').html().trim();
                }
            }

            return '';
        }

        this.getCountUnviewed = function(id, type){

            if (undefined!=id && ''!=id){

                if ('category'==type){
                    var a = $('#nav-categories li a.category-link[data-id="'+id+'"]');
                    if (undefined!=a){
                        return a.find('.count-unviewed em').html().trim();
                    }
                }

                if ('feed'==type){
                    var a = $('#nav-categories li a.feed-link[data-id="'+id+'"]');
                    if (undefined!=a){
                        return a.find('.count-unviewed em').html().trim();
                    }
                }
            }

            return $('#all-posts .count-unviewed em').html().trim();
        }
    }

    return new App();
})