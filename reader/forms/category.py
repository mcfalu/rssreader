from django import forms


class CategoryForm(forms.Form):

    title = forms.CharField(max_length=255)
    color = forms.CharField(required=False, widget=forms.HiddenInput(attrs={'class':'form-control'}))
