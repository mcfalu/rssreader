from django import forms
from django.forms.util import ErrorList

from mongoengine.django.auth import User

class UserForm(forms.Form):

    user = None

    def __init__(self, config, *args, **kwargs):

        if 'user' in config:
            self.user = config['user']

        super(UserForm, self).__init__(*args, **kwargs)

class AccountForm(UserForm):

    username = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    first_name = forms.CharField(max_length=255, required=False)
    last_name = forms.CharField(max_length=255, required=False)
    email = forms.EmailField()

class ChangePasswordForm(UserForm):

    old_password = forms.CharField(max_length=255, widget = forms.PasswordInput())
    new_password = forms.CharField(max_length=255, widget = forms.PasswordInput())
    new_password_confirmation = forms.CharField(max_length=255, widget = forms.PasswordInput())

    def clean(self):

        cleaned_data = super(ChangePasswordForm, self).clean()

        if self.user:

            old_password = cleaned_data.get('old_password')
            if None != old_password and not self.user.check_password(old_password):

                msg = "Invalid old password"
                self._errors['old_password'] = ErrorList([msg])

        new_password = cleaned_data.get('new_password')
        new_password_confirmation = cleaned_data.get('new_password_confirmation')

        if None != new_password and new_password != new_password_confirmation:

            msg = "Invalid confirmation of new password"
            self._errors['new_password_confirmation'] = ErrorList([msg])

        return cleaned_data

class ChangeUsernameForm(UserForm):

    username = forms.CharField(max_length=255, widget = forms.TextInput())

    def clean(self):

        cleaned_data = super(ChangeUsernameForm, self).clean()

        if self.user:

            username = cleaned_data.get('username')

            user = User.objects(username=username).first()
            if None is not user and user.id != self.user.id:

                msg = "This username is already used"
                self._errors['username'] = ErrorList([msg])

        return cleaned_data