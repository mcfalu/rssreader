from django import forms
from django.forms.util import ErrorList

from reader.core import feeds

class FeedForm(forms.Form):

    title = forms.CharField(max_length=255)
    category = forms.ChoiceField()
    site_url = forms.URLField(max_length=255)
    link = forms.URLField(max_length=255)

    user_id = None
    feed_id = None

    def __init__(self, config, *args, **kwargs):

        if 'user_id' in config:
            self.user_id = config['user_id']
        if 'feed_id' in config:
            self.feed_id = config['feed_id']

        super(FeedForm, self).__init__(*args, **kwargs)

        if 'category_options' in config:
            self.fields['category'].choices = config['category_options']

    def clean(self):

        cleaned_data = super(FeedForm, self).clean()

        if self.user_id:

            link = cleaned_data.get('link')

            if '' != link and feeds.is_exist_feed(link, self.feed_id, self.user_id):

                msg = "There is already exist feed with this link"
                self._errors['link'] = ErrorList([msg])

        return cleaned_data


class FeedsFilterForm(forms.Form):
    category = forms.ChoiceField(required=False)

    def __init__(self, config, *args, **kwargs):

        super(FeedsFilterForm, self).__init__(*args, **kwargs)

        if 'category_options' in config:
            self.fields['category'].choices = config['category_options']