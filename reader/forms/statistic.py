import calendar
from datetime import datetime

from django import forms

class FilterForm(forms.Form):

    month = forms.ChoiceField(required=False)
    year = forms.ChoiceField(required=False)


    def __init__(self, *args, **kwargs):

        super(FilterForm, self).__init__(*args, **kwargs)

        # generate options for field month
        months_choices = []
        for i in range(1,13):
            months_choices.append((i, calendar.month_name[i]))

        self.fields['month'].choices = months_choices

        # generate options for field year
        currentYear = datetime.now().year

        year_choices = []
        year = 2012
        while year <= currentYear:

            year_choices.append((year, year))
            year = year +1

        self.fields['year'].choices = year_choices