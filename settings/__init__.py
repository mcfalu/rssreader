from common import *

#try to load local settings
try:
    from local import *
except Exception, e:
    pass

TEMPLATE_DEBUG = DEBUG

# Connect to MongoDB
if 'test' in sys.argv:

    # Connect to test database
    mongoengine.connect(DATABASES_MONGO['test']['NAME'],
                        host=DATABASES_MONGO['test']['HOST'],
                        port=DATABASES_MONGO['test']['PORT'])

    # Change settings for SQL database
    DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'
    DATABASES['default']['NAME'] = os.path.join(PROJECT_DIR, 'tests_data/test.db')

else:

    mongoengine.connect(DATABASES_MONGO['default']['NAME'],
                        host=DATABASES_MONGO['default']['HOST'],
                        port=DATABASES_MONGO['default']['PORT'])

# If Debug mode is turned on
if DEBUG_TOOLBAR:

    INTERNAL_IPS = ('127.0.0.1')

    MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + ('debug_toolbar.middleware.DebugToolbarMiddleware', )

    INSTALLED_APPS = INSTALLED_APPS + ('debug_toolbar', )

    DEBUG_TOOLBAR_PANELS = (
        'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        'debug_toolbar.panels.headers.HeaderDebugPanel',
        'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        'debug_toolbar.panels.template.TemplateDebugPanel',
        'debug_toolbar.panels.logger.LoggingPanel',
    )

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }