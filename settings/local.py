DATABASES_MONGO = {
    'default' : {
        'NAME' : 'rss-reader',
        'HOST' : 'localhost',
        'PORT' : 27017,
    },
    'test' : {
        'NAME' : '[DB-TEST]',
        'HOST' : 'localhost',
        'PORT' : 27017,
    }
}

DEBUG_TOOLBAR = False

DEBUG = True

ALLOWED_HOSTS = ['reader']