# user local system defines
ADMINPATH=~/db-backup/rss-reader
MONGO_DBNAME=rss-reader

DAY=`/bin/date +%Y%m%d_%H%M%S`

echo "Creating MONGO DUMP: mongo_$DAY.tar"
mongodump --db $MONGO_DBNAME --out $ADMINPATH/dumps/mongo_$DAY
cd $ADMINPATH/dumps/mongo_$DAY/
tar -cvzf "$ADMINPATH/dumps/mongo_$DAY.tar" $MONGO_DBNAME/

cd $ADMINPATH
rm -rf $ADMINPATH/dumps/mongo_$DAY